/* ******************************************************************
* Constantes de configuration
* ****************************************************************** */
const apiKey = ""; //"69617e9b-19db-4bf7-a33f-18d4e90ccab7"; //"650b8a2a-a5d5-4b80-a22d-33ce5f256d4a";
const serverUrl = "https://lifap5.univ-lyon1.fr";

/* ******************************************************************
* Gestion de la boîte de dialogue (a.k.a. modal) d'affichage de
* l'utilisateur.
* ****************************************************************** */

/**
 * Fait une requête GET authentifiée sur /whoami
 * @param {API} apiKey recuperer
 * @returns une promesse du login utilisateur ou du message d'erreur
 */
function fetchWhoami(API) {
  return fetch(serverUrl + "/whoami", { headers: { "Api-Key":  API} })
    .then((response) => {
      if (response.status === 401) {
        return response.json().then((json) => {
        console.log(json);
        return { err: json.message };
      });
    } else {
      return response.json();
    }
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête GET authentifiée sur /pokemon
 * @returns une promesse de la liste de pokemon ou du message d'erreur
 */
function fetchPokemon() {
  return fetch(serverUrl + "/pokemon", { headers: { "Api-Key": apiKey } })
    .then((response) => {
      if (response.status === 401) {
        return response.json().then((json) => {
          console.log(json);
          return { err: json.message };
        });
      } else {
        return response.json();
      }
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête sur le serveur et insère le login dans la modale d'affichage
 * de l'utilisateur puis déclenche l'affichage de cette modale.
 * @param {Etat} etatCourant l'état courant
 * @returns Une promesse de mise à jour
 */
function lanceWhoamiEtInsereLogin(etatCourant) {
  return fetchWhoami(etatCourant.apiKey).then((data) => {
    majEtatEtPage(etatCourant, {
      login: data.user, // qui vaut undefined en cas d'erreur
      errLogin: data.err, // qui vaut undefined si tout va bien
      loginModal: true, // on affiche la modale
    });
  });
}

/**
 * Fonction qui retourne l'html pour afficher un champ pour le password et le verifie
 * @param {Etat} etatCourant 
 * @returns html pour mettre le champ password et traiter les erreurs
 */
function genereHtmlForPassword(etatCourant){
  return `<form>
  <label for="apiKey">apiKey : </label>
  <input type="password" id="apikey">
  <p style ="color : red"> ${etatCourant.errLogin !== undefined
                            ? etatCourant.errLogin : ""}
  </form>`
}

/**
 * Génère le code HTML du corps de la modale de login. On renvoie en plus un
 * objet callbacks vide pour faire comme les autres fonctions de génération,
 * mais ce n'est pas obligatoire ici.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et un objet vide
 * dans le champ callbacks
 */
function genereModaleLoginBody(etatCourant) {
  const text =
    etatCourant.login === undefined
      ? genereHtmlForPassword(etatCourant)
      : etatCourant.login;
  return {
    html: `
  <section class="modal-card-body">
    <p>${text}</p>
  </section>
  `,
    callbacks: {},
  };
}

/**
 * Génère le code HTML du titre de la modale de login et les callbacks associés.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLoginHeader(etatCourant) {
  return {
    html: `
<header class="modal-card-head  is-back">
  <p class="modal-card-title">Utilisateur</p>
  <button
    id="btn-close-login-modal1"
    class="delete"
    aria-label="close"
    ></button>
</header>`,
    callbacks: {
      "btn-close-login-modal1": {
        onclick: () => majEtatEtPage(etatCourant, { loginModal: false }),
      },
    },
  };
}

/**
 * Génère le code HTML du bas de la modale de login et les callbacks associés.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML 
 */
function genererHtmlModaleLoginFooter(etatCourant){
  return `
  <footer class="modal-card-foot" style="justify-content: flex-end">
  ${etatCourant.login === undefined ?
  "<button id='btn-valider-login-modal' class='button'>Valider</button>"
  :""}
  <button id="btn-close-login-modal" class="button">Fermer</button>
  </footer>
  `
}

/**
 * Recupere le code HTML du bas de page de la modale de login et genere les callbacks associés.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLoginFooter(etatCourant) {
  htmlfooter = genererHtmlModaleLoginFooter(etatCourant);
  return {
    html: htmlfooter ,
    callbacks: {
      "btn-close-login-modal": {
        onclick: () => majEtatEtPage(etatCourant, { loginModal: false }),
      },
     "btn-valider-login-modal" :{
        onclick :() => recupPasswordAndConnexion(etatCourant),
     },
    },
  };
}

/**
 * Fonction qui récupere le password inserer pour l'apiKey
 * et qui lance le whoami et insere le login
 * @param {Etat} etatCourant 
 */
function recupPasswordAndConnexion(etatCourant){
  login = document.getElementById("apikey").value;
  etatCourant.apiKey = login;
  lanceWhoamiEtInsereLogin(etatCourant);
}

/**
 * Génère le code HTML de la modale de login et les callbacks associés.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLogin(etatCourant) {
  const header = genereModaleLoginHeader(etatCourant);
  const footer = genereModaleLoginFooter(etatCourant);
  const body = genereModaleLoginBody(etatCourant);
  const activeClass = etatCourant.loginModal ? "is-active" : "is-inactive";
  return {
    html: `
      <div id="mdl-login" class="modal ${activeClass}">
        <div class="modal-background"></div>
        <div class="modal-card">
          ${header.html}
          ${body.html}
          ${footer.html}
        </div>
      </div>`,
    callbacks: { ...header.callbacks, ...footer.callbacks, ...body.callbacks },
  };
}

/* ************************************************************************
 * Gestion de barre de navigation contenant en particulier les bouton Pokedex,
 * Combat et Connexion.
 * ****************************************************************** */

/**
 * Déclenche la mise à jour de la page en changeant l'état courant pour que la
 * modale de login soit affichée
 * @param {Etat} etatCourant
 */
function afficheModaleConnexion(etatCourant) {
  lanceWhoamiEtInsereLogin(etatCourant);
}


/**
 * Génère le code HTML et les callbacks pour le bouton connexion 
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBoutonConnexion(etatCourant) {
  const html = `
  <div class="navbar-end">
    <div class="navbar-item">
      <div class="buttons">
        <a id="btn-open-login-modal" class="button is-light"> Connexion </a>
      </div>
    </div>
  </div>`;
  return {
    html: html,
    callbacks: {
      "btn-open-login-modal": {
        onclick: () => afficheModaleConnexion(etatCourant),
      },
    },
  };
}

/**
 * Génère le code HTML et les callbacks pour le bouton deconnexion 
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBoutonDeconnexion(etatCourant) {
  const html = `
  <div class="navbar-end">
    <div class="navbar-item">
      <div class="buttons">
        <a id="btn-open-deco-modal" class="button is-light"> Deconnexion </a>
        <p> Utlisateur : ${etatCourant.login}</p>
      </div>
    </div>
  </div>`;
  return {
    html: html,
    callbacks: {
      "btn-open-deco-modal": {
        onclick: () => majEtatEtPage(etatCourant, {apiKey : undefined,
                                                  login : undefined}),
      },
    },
  };
}

/** 
 * La fonction met a jour la page avec la liste de pokemon 
 * ainsi que les champs d'etatCourant à initialiser
 * @param {Etat} etatCourant
 */
function recuperePokemon(etatCourant) {
  return fetchPokemon() 
  .then(json => majEtatEtPage(etatCourant,{
    NbPokemon:10,
	TriPok:1,
	ordreTri:1,
	search:'',
	pokemons:json,
	PokSelect:json[18],
  }));
}

/** 
 * Fonction qui créer un callback pour changer le pokemon sélectionné
 * @param {Etat} etatCourant, {poke} pokemon sectionné
 * @returns la description des callbacks à enregistrer dans le champ callbacks
 * et le callback
 */
function CallbackForPokSelect(etatCourant, poke)
{
  const callback = {};
  callback[`tabLigne${poke.PokedexNumber}`] 
  = {"onclick": () => majEtatEtPage(etatCourant,{PokSelect:poke}) };
  return callback;
}

/** 
 * Fonction qui change l'affichage du pokemon selectionné
 * @param {Etat} etatCourant, {pokemon} pokemon sectionné
 * @returns la description des callbacks à enregistrer dans le champ callbacks
 * et le callback
 */
function genereClassSelected(pokemon, etatCourant)
{
  return (pokemon === etatCourant.PokSelect) ? "is-selected" : ''; 
}

/** 
 * Fonction qui créer un callback pour changer le type de triage
 * @param {Etat} etatCourant
 * @returns la description des callbacks à enregistrer dans le champ callbacks
 */
function CallbackTriage(etatCourant)
{
  if(etatCourant.TriPok === undefined)
  {
    return {html:"",
    callbacks:{}}
  }
  else
  {
    return {html:"",
    callbacks:  {
      "tri_id": {"onclick": () => majEtatEtPage(etatCourant
      ,{TriPok:1,ordreTri:etatCourant.ordreTri + 1}) }
    }}
}}

/** 
 * Fonction qui tri le tab de pokemon par numero de pokedex
 * @param {sens_tri} sens du tri, {tabPokemon} tableau de pokemon
 * @returns tabPokemon trier 
 */
function TriNumPokedex(sens_tri, tabPokemon)
{
  return (sens_tri % 2 === 1 ? tabPokemon.sort( 
        (a,b) => a.PokedexNumber-b.PokedexNumber)
        : tabPokemon.sort( 
          (a,b) => b.PokedexNumber-a.PokedexNumber));
}

/** 
 * Fonction qui tri le tableau de pokemon en fonction de tri voulu
 * @param {sens_tri} sens du tri, {tabPokemon} tableau de pokemon, 
 * {TriPok} type de triage
 * @returns tabPokemon trier en fonction du type de tri 
 */
function TriPokemon(TriPok, sens_tri, tabPokemon)
{
    if(TriPok === 1) //On tri par n°Pokedex
    {
      return TriNumPokedex(sens_tri, tabPokemon);
    }
}

/** 
 * Fonction qui crée une ligne du tableau de pokemon
 * @param {n} pokemon, {Etat} etatCourant
 * @returns html ligne de pokemon  
 */
function genereLigneTabPokemon(n, etatCourant){
  return ` <tr id="tabLigne${n.PokedexNumber}"
    class="${genereClassSelected(n,etatCourant)}"> 
    <td>
    <img
    alt="${n.Name}"
    src="${n.Images.Detail}"
    width="64"
    />
    </td>
    <td><div class="content">${n.PokedexNumber}</div></td>
    <td><div class="content">${n.Name}</div></td>
    <td><ul>
    ${n.Abilities.map( (a) => `<li> ${a} </li>`).join('\n')}
    </ul></td>
    <td><ul>
    ${n.Types.map( (a) => `<li> ${a} </li>`).join('\n')}
    </ul></td>
	</tr>`;
}

/** 
 * Fonction qui crée une ligne avec les details du tableau de pokemon
 * @param {html} html a integrer au code 
 * @returns html details du tableau de pokemon  
 */
function genereTabDetails(html){
  const details = 
  `<div class="tabs is-centered">
  <ul><li class="is-active" id="tab-all-pokemons"><a>Tous les pokemons</a></li>
  <li id="tab-tout"><a>Mes pokemons</a></li></ul></div>
  <div id="tbl-pokemons">
  <table class="table">
  <div style ="display : inline-flex">
  <input class ="input" placeholder="Rechercher Un Pokemon"
  type="text" value="" id="RecherchePokemon"></div>
  <thead><tr>
  <th><span>Image</span></th>
  <th><span id="tri_id">#</span><span class="icon"><i class="fas fa-angle-up">
  </i></span></th>
  <th><span id="tri_name">Name</span></th>
  <th><span id="tri_abilities">Abilities</span></th>
  <th><span id="tri_types">Types</span></th>
  </tr></thead>
  <tbody>`  + html + `</tbody></table></div>`
  return details;
}

/** 
 * Fonction qui genere l'html tableau de pokemon
 * @param {Etat} etatCourant
 * @returns html tableau de pokemon  
 */
function genereTabPokemon(etatCourant) {
  if(etatCourant.pokemons !== undefined)
  {
	const filterTabPokemon=filterPokemon(etatCourant);
    const html = TriPokemon(etatCourant.TriPok, etatCourant.ordreTri, 
	etatCourant.search===undefined ?
    etatCourant.pokemons : filterTabPokemon)
    .slice(0,etatCourant.NbPokemon).map( (n) => 
	genereLigneTabPokemon(n,etatCourant)).join('');
    const html2 = genereTabDetails(html);
    const LigneTab = etatCourant.search===undefined ? etatCourant.pokemons.
	slice(0,etatCourant.NbPokemon).map( (n) => 
	CallbackForPokSelect(etatCourant,n)):filterTabPokemon
    .slice(0,etatCourant.NbPokemon)
    .map((n)=>CallbackForPokSelect(etatCourant,n));
    const callbacks = LigneTab.reduce( (acc,l) => ( {...acc, ...l }), {});
    return {html:html2,
      callbacks:callbacks,
    };
  }else { return {html:"Pas de pokemon",callbacks:{}}; }
}

/** 
 * Fonction qui génère l'html du haut de la carte de pokemon
 * @param {pokemon} pokemon
 * @returns html haut de la carte de pokemon  
 */
function genereTitreCarte(pokemon) {
  return `
  <div class="card-header">
    <div class="card-header-title">${pokemon.JapaneseName} 
    (#${pokemon.PokedexNumber})</div>
  </div>
  <div class="card-content">
    <article class="media">
      <div class="media-content">
        <h1 class="title">${pokemon.Name}</h1>
      </div>
    </article>
  </div>`
}

/** 
 * Fonction qui génère le code html du contenu de la carte du pokemon choisi
 * @param {pokemon} pokemon, {tab_res} tableau de resistance, {tab_str} tableau des weak
 * @returns html contenu de la carte de pokemon  
 */
 function genereContenuCartePokemon(pokemon,tab_res,tab_str)
 {
   return `
   <div class="media-content">
      <div class="content has-text-left">
        <p>Hit points: ${pokemon.Hp}</p>
        <h3>Abilities</h3>
          <ul>
            ${pokemon.Abilities.map( (a) => `<li> ${a} </li>`).join('\n')}
          </ul>
          <h3>Resistant against</h3>
          <ul>
            ${tab_res.map( (r) => `<li> ${r} </li>`).join('')}
          </ul>
          <h3>Weak against</h3>
          <ul>
            ${tab_str.map( (r) => `<li> ${r} </li>`).join('')}
          </ul>
      </div>
    </div>`
 }

 /**Fonction qui génère le code html pour afficher l'image d'un pokemon
 * @param pokemon
 * @returns du code html
 */
  function genereImagePokemon(pokemon)
  {
    return `
    <figure class="media-right">
      <figure class="image is-475x475">
        <img
          class=""
          src="${pokemon.Images.Full}"
          alt="${pokemon.Name}"
        />
      </figure>
    </figure>`
  }

  /**Fonction qui génère le code html pour afficher le pokemon sélectionné 
   * @param pokemon
   * @returns du code html
   */
  function genereSelectedPokemonHtml(pokemon)
  {
    const against_tab = Object.keys(pokemon.Against); //On transforme l'objet against en tableau
    const against_tab_res = against_tab.filter( (n) => 
    pokemon.Against[n] < 1); //On récupère un tableau avec les types efficaces
    const against_tab_str = against_tab.filter( (n) => 
    pokemon.Against[n] > 1);
    return `
    <div class="columns">
     <div class="card" id="card" style="right:0px; position:fixed;">
       ${genereTitreCarte(pokemon)}
       <div class="card-content">
         <article class="media">
          ${genereContenuCartePokemon(pokemon,against_tab_res,against_tab_str)}
          ${genereImagePokemon(pokemon)}
         </article>
       </div>
     </div>
    </div>`
  }


/**Fonction qui génère l'html du pokemon courant si PokSelect est défini
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html
 */
function genereSelectedPokemon(etatCourant) {
 
  if(etatCourant.PokSelect !== undefined)
  {
  return {html:genereSelectedPokemonHtml(etatCourant.PokSelect),
      callbacks: {}};
  }
    else
    {
      return {html:"Pas de pokemon",callbacks:{}};
    }
}

/**
 * Génère le code HTML du bouton permettant de changer le nombre de pokemon afficher
 * @param {*} etatCourant 
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBoutonLimitationPokemon(etatCourant) {
  const html = ` <a id ="lessPok" class="button is-light">Moins</a>
    <a id="morePok" class="button is-light">Plus</a> `;
  if(etatCourant.NbPokemon !== undefined)
  {
    const callbacks = {
      "morePok": {
        onclick: () => { majEtatEtPage(etatCourant,
		{NbPokemon:etatCourant.NbPokemon + 10}) }
      },
      "lessPok":{
        onclick: () => { if(etatCourant.NbPokemon > 10) 
		{ majEtatEtPage(etatCourant,{NbPokemon:etatCourant.NbPokemon - 10}) } }
      }
    }
    return {html:html, callbacks:callbacks};
  }
  else { return {html:html,callbacks:{}}; }
}

/**
 * Fonction qui permet de rechercher un pokemon
 * @param {Etat} etatCourant
 * @returns la description des callbacks à enregistrer dans le champ callbacks
 */
function genereSearchPokemon(etatCourant) {
  return {
    callbacks: {
      "RecherchePokemon": {
        onchange: () =>{
          document.getElementById("RecherchePokemon").value !=='' ? 
          searchPokemon(etatCourant,document.getElementById("RecherchePokemon")
		  .value):
          searchPokemon(etatCourant,'');
        },
      },
    },
  };
}

/**
 * Fonction qui met à jour la page avec les pokemon qui ont un nom qui correspond
 * au recherche permet de rechercher un pokemon
 * @param {Etat} etatCourant, {pokemonName} nom du pokemon
 */
function searchPokemon(etatCourant, pokemonName=''){
  majEtatEtPage(etatCourant,{search: pokemonName});
}

/**
 * Fonction qui filtre les pokemon en fonction du nom de la recherche
 * @param {Etat} etatCourant
 * @returns la mise a jour des champs de l'etatCourant
 */
function filterPokemon(etatCourant){
  return etatCourant.pokemons
  .filter((pokemon)=>pokemon.Name.toLowerCase()
  .search(etatCourant.search.toLowerCase())>=0);
}

/**
 * Génère le code HTML de la barre de navigation et les callbacks associés.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBarreNavigation(etatCourant) {
  const connexion = etatCourant.apiKey === undefined ||//Si on a rien dans la clé API
                    etatCourant.login === undefined? // ou si elle est mauvaise
                    genereBoutonConnexion(etatCourant) //on affiche la connexion
                    : genereBoutonDeconnexion(etatCourant);
  return {
    html: `
  <nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="navbar">
      <div class="navbar-item"><div class="buttons">
          <a id="btn-pokedex" class="button is-light"> Pokedex </a>
          <a id="btn-combat" class="button is-light"> Combat </a>
      </div></div>
      ${connexion.html}
    </div>
  </nav>`,
    callbacks: {
      ...connexion.callbacks,
      "btn-pokedex": { onclick: () => console.log("click bouton pokedex") },
    },
  };}

/**
 * Génére le code HTML de la page ainsi que l'ensemble des callbacks à
 * enregistrer sur les éléments de cette page.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function generePage(etatCourant) {
  const barredeNavigation = genereBarreNavigation(etatCourant);
  const modaleLogin = genereModaleLogin(etatCourant);
  const LimitePokemon = genereBoutonLimitationPokemon(etatCourant);
  const SelectedPokemon = genereSelectedPokemon(etatCourant);
  const ListePokemon = genereTabPokemon(etatCourant);
  const tri=CallbackTriage(etatCourant);
  const search=genereSearchPokemon(etatCourant);
  // Attention, les callbacks définis dans modaleLogin.callbacks vont écraser
  // ceux définis sur les mêmes éléments dans barredeNavigation.callbacks. En
  // pratique ce cas ne doit pas se produire car barreDeNavigation et
  // modaleLogin portent sur des zone différentes de la page et n'ont pas
  // d'éléments en commun.
  return {
    html: barredeNavigation.html + modaleLogin.html + SelectedPokemon.html 
    + ListePokemon.html + LimitePokemon.html + tri.html,
    callbacks: { ...barredeNavigation.callbacks, ...modaleLogin.callbacks, 
      ...SelectedPokemon.callbacks, ...ListePokemon.callbacks, 
      ...LimitePokemon.callbacks, ...tri.callbacks, ...search.callbacks},
  }; // ... permet de fusionner les dictionnaire de callback
}

/* ******************************************************************
 * Initialisation de la page et fonction de mise à jour
 * globale de la page.
 * ****************************************************************** */

/**
 * Créée un nouvel état basé sur les champs de l'ancien état, mais en prenant en
 * compte les nouvelles valeurs indiquées dans champsMisAJour, puis déclenche la
 * mise à jour de la page et des événements avec le nouvel état.
 * @param {Etat} etatCourant etat avant la mise à jour
 * @param {*} champsMisAJour objet contenant les champs à mettre à jour, ainsi
 * que leur (nouvelle) valeur.
 */
function majEtatEtPage(etatCourant, champsMisAJour) {
  const nouvelEtat = { ...etatCourant, ...champsMisAJour };
  majPage(nouvelEtat);
}

/**
 * Prend une structure décrivant les callbacks à enregistrer et effectue les
 * affectation sur les bon champs "on...". Par exemple si callbacks contient la
 * structure suivante où f1, f2 et f3 sont des callbacks:
 *
 * { "btn-pokedex": { "onclick": f1 },
 *   "input-search": { "onchange": f2,
 *                     "oninput": f3 }
 * }
 *
 * alors cette fonction rangera f1 dans le champ "onclick" de l'élément dont
 * l'id est "btn-pokedex", rangera f2 dans le champ "onchange" de l'élément dont
 * l'id est "input-search" et rangera f3 dans le champ "oninput" de ce même
 * élément. Cela aura, entre autres, pour effet de délclencher un appel à f1
 * lorsque l'on cliquera sur le bouton "btn-pokedex".
 *
 * @param {Object} callbacks dictionnaire associant les id d'éléments à un
 * dictionnaire qui associe des champs "on..." aux callbacks désirés.
 */
function enregistreCallbacks(callbacks) {
  Object.keys(callbacks).forEach((id) => {
    const elt = document.getElementById(id);
    if (elt === undefined || elt === null) {
      console.log(
      `Élément inconnu: ${id}, impossible d'enregistrer de callback sur cet id`
      );
    } else {
      Object.keys(callbacks[id]).forEach((onAction) => {
        elt[onAction] = callbacks[id][onAction];
      });
    }
  });
}

/**
 * Mets à jour la page (contenu et événements) en fonction d'un nouvel état.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majPage(etatCourant) {
  console.log("CALL majPage",etatCourant);
  const page = generePage(etatCourant);
  document.getElementById("root").innerHTML = page.html;
  enregistreCallbacks(page.callbacks);
}

/**
 * Appelé après le chargement de la page.
 * Met en place la mécanique de gestion des événements
 * en lançant la mise à jour de la page à partir d'un état initial.
 */
function initClientPokemons() {
  console.log("CALL initClientPokemons");
  const etatInitial = {
    loginModal: false,
    login: undefined,
    errLogin: undefined
    
  };
  recuperePokemon(etatInitial);
}

// Appel de la fonction init_client_duels au après chargement de la page
document.addEventListener("DOMContentLoaded", () => {
  console.log("Exécution du code après chargement de la page");
  initClientPokemons();
});